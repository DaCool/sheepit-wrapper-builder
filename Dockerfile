FROM debian:stable-slim

# Maintainer
LABEL maintainer=DaCoolX

ARG DEBIAN_FRONTEND=noninteractive
# Makes the apt-get process aware
# of the docker build process env
# so it doesn't complain as much

RUN \
	# Install wget, unzip and 7zip deps
	apt-get update && \
	apt-get upgrade -y && \
	apt-get install -y --no-install-recommends \
		wget \
		ca-certificates \
		unzip \
		p7zip && \
	apt-get -y autoremove && \
	apt-get -y clean && \
	rm -rf \
		/var/lib/apt/lists/* \
		/tmp/*

WORKDIR /build
# jvm is taken from https://adoptium.net/releases.html?variant=openjdk11&jvmVariant=hotspot
ARG jvm_name="jdk-11.0.13+8-jre"
ADD $jvm_name.zip /build/
# Creating folder structure
RUN mkdir /build/jre
# Downloading client to the target directory
RUN wget https://www.sheepit-renderfarm.com/media/applet/client-latest.php -O jre/sheepit-client.jar
# Unziping jre
RUN \
	unzip $jvm_name.zip && \
	cp -rf $jvm_name/* jre/
# Compressing app package
WORKDIR /build/jre
RUN 7zr a -mx=9 /build/application.7z ./
# Building the exe bundle and cleaning up
WORKDIR /build
ADD starter.sfx /build/
ADD config.cfg /build/
RUN \
	cat starter.sfx config.cfg application.7z > sheepit-wrapper.exe && \
	rm -rf application.7z jre $jvm_name
